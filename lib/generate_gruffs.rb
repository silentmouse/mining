require 'gruff'
require "parquet"


class GenerateGruffs


  class << self


    def create_all

      @reports = Dir.glob(Rails.root.join('public', 'reports','*')).map{|t| t.split("/").last.split(".").first}

      @reports.each do |report|
        create(report)
      end

    end


    def create(report = "second")


      table = Arrow::Table.load("#{Rails.root}/public/reports/#{report}.parquet")

      count = table.size

      rh = table["restored_height"].to_a
      cd = table["current_distance"].to_a
      ci = table["cluster_id"].to_a
      ciu = table["cluster_id"].to_a.uniq


      cluster = ciu.first

      t = Time.now

      i = 1

      @xy = []

      while i < count - 1

        begin

          if rh[i].present? && cd[i].present? && ci[i] == cluster
            h = rh[i]
            d = cd[i]
            @xy << {distance: d, height: h}
          end

          i = i + 1

          if cluster != ci[i + 1]

            cluster_name = cluster.split(".").first

            puts "TIME = #{Time.now - t} CLUSTER = #{cluster} before"

            puts "0"
            g = Gruff::Line.new
            g.theme_pastel
            g.title = 'Profile'
            labels = {}
            puts "1"
            @xy.each {|t| labels[t[:distance].to_s] = t[:distance]}
            g.labels = labels
            puts "2"
            g.data cluster_name.to_sym, @xy.map {|t| t[:height]}

            puts "3"

            sleep 10

            g.write("public/gruff_images/#{report}__#{cluster_name}.png")

            puts "TIME = #{Time.now - t} CLUSTER = #{cluster}"



            @xy = []

            cluster = ci[i + 1]
          end

        rescue => err

           puts "ERR = #{err}"

        end

      end

    #   puts "TIME = #{Time.now - t} CLUSTER = #{cluster} before"
    #
    #   g = Gruff::Line.new
    #   g.theme_pastel
    #   g.title = 'Profile'
    #   labels = {}
    #   @xy.each {|t| labels[t[:distance].to_s] = t[:distance]}
    #   g.labels = labels
    #   g.data cluster.to_sym, @xy.map {|t| t[:height]}
    #   g.write("public/gruff_images/#{report}__#{cluster}.png")
    #
    #   puts "TIME = #{Time.now - t} CLUSTER = #{cluster}"
    #
    #
    #   ciu.each do |cluster|
    #
    #     t = Time.now
    #
    #     i = 1
    #
    #     @xy = []
    #
    #     while i < count - 1
    #
    #       if rh[i].present? && cd[i].present? && ci[i] == cluster
    #         h = rh[i]
    #         d = cd[i]
    #         @xy << {distance: d, height: h}
    #       end
    #       i = i + 1
    #     end
    #
    #     puts "TIME = #{Time.now - t} CLUSTER = #{cluster} before"
    #
    #     g = Gruff::Line.new
    #     g.theme_pastel
    #     g.title = 'Profile'
    #     labels = {}
    #     @xy.each {|t| labels[t[:distance].to_s] = t[:distance]}
    #     g.labels = labels
    #     g.data cluster.to_sym, @xy.map {|t| t[:height]}
    #     g.write("public/gruff_images/#{report}__#{cluster}.png")
    #
    #     puts "TIME = #{Time.now - t} CLUSTER = #{cluster}"
    #
    #   end
    #
    end

  end
end
