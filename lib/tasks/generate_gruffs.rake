require "generate_gruffs"

namespace :generate_gruffs do

  task create_all: :environment do
    GenerateGruffs.create_all
  end

end

