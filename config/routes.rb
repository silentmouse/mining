Rails.application.routes.draw do

  resources :roads, only: [:index,:show]

  resources :reports, only: [:index] do
    resources :profiles, only: [:show,:index]
  end

  get '/profiles/:id/history', to: 'profiles#history'

  resources :people

  root to: 'welcome#index'

end
