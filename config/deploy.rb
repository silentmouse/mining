# server 'whats', port: 6000, roles: [:web, :app, :db], primary: true, user: "rbdev"
#
set :repo_url,        'git@bitbucket.org:silentmouse/mining.git'
# set :repo_url,        '"git@github.com:Solver-Club/wb-support.git"'
set :application,     'mining'
set :user,            'rbdev'
set :puma_threads,    [4, 16]
set :puma_workers,    0

set :linked_files, fetch(:linked_files, []).push('config/database.yml','config/master.key')
set :linked_dirs, fetch(:linked_dirs, []).push('config/thin', 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system','public/uploads')

# Don't change these unless you know what you're doing
set :pty,             true
set :tmp_dir,         "/home/#{fetch(:user)}/tmp/capistrano"
set :use_sudo,        false
set :stage,           :production
set :deploy_via,      :remote_cache
set :deploy_to,       "/home/#{fetch(:user)}/#{fetch(:application)}"
set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :ssh_options,     { forward_agent: true, user: fetch(:user), keys: %w(~/.ssh/id_rsa.pub) }
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true  # Change to false when not using ActiveRecord

## Defaults:
# set :scm,           :git
# set :branch,        :master
# set :format,        :pretty
# set :log_level,     :debug
# set :keep_releases, 5

## Linked Files & Directories (Default None):
# set :linked_files, %w{config/database.yml}
# set :linked_dirs,  %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end

  before :start, :make_dirs
end




# before "deploy:assets:precompile", "deploy:yarn_install"
#
# namespace :deploy do
#   desc 'Run rake yarn:install'
#   task :yarn_install do
#     on roles(:web) do
#       within release_path do
#         execute("cd #{release_path} && yarn install")
#       end
#     end
#   end
# end

# namespace :deploy do
#   namespace :assets do
#     task :install_webpack do
#       on roles(:app) do
#         within release_path do
#           with rails_env: fetch(:stage) do
#             execute :rake , "webpacker:binstubs"
#           end
#         end
#       end
#     end
#     before :precompile, 'assets:install_webpack'
#   end
# end

namespace :deploy do
  desc "Make sure local git is in sync with remote."
  puts "*"*5 + " PRODUCTION 0" + "*"*5
  task :check_revision do
    puts "*"*5 + " PRODUCTION 2222" + "*"*5
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end
  puts "*"*5 + " PRODUCTION 1" + "*"*5
  desc 'Initial Deploy'
  task :initial do
    puts "*"*5 + " PRODUCTION 2" + "*"*5
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end



  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
    end
  end

  before :starting,     :check_revision
  after  :finishing,    :compile_assets
  after  :finishing,    :cleanup
  after  :finishing,    :restart
end
#
# # ps aux | grep puma    # Get puma pid
# # kill -s SIGUSR2 pid   # Restart puma
# # kill -s SIGTERM pid   # Stop puma

# config valid for current version and patch releases of Capistrano


# set :application, "wb_auth"
# set :repo_url, "git@github.com:Solver-Club/wb_auth.git"
#
# # Default branch is :master
# # ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
#
# # Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/home/rbdev/wb_auth'
#

