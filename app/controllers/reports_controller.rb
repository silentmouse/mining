class ReportsController < ApplicationController
  def index

    @reports = Dir.glob(Rails.root.join('public', 'reports','*')).map{|t| {name: t.split("/").last.split(".").first}}

    render json: {data: @reports}

  end
end
