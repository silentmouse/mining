require "parquet"

class ProfilesController < ApplicationController

  def index
    table = Arrow::Table.load("#{Rails.root}/public/reports/#{params[:report_id]}.parquet")
    @ci = table["cluster_id"].to_a.uniq
    render json: {data: @ci.map{|t| t.split(".").first}}
  end

  def history

    id = params[:id]
    @profiles = Dir.glob(Rails.root.join('public', 'gruff_images','*')).map{|t| {name: t.split("/").last.split(".").first}}
    @profiles = Dir.glob(Rails.root.join('public', 'gruff_images','*')).map{|t| {name: t.split("/").last.split(".").first}}

    @profiles = @profiles.map{|t| (t[:name].split("__").last == id ? t[:name] : nil)}
    @profiles.delete(nil)

    render json: {data: @profiles.uniq.sort()}

  end


  def show

    table = Arrow::Table.load("#{Rails.root}/public/reports/#{params[:report_id]}.parquet")

    count = table.size

    @xy = []

    i = 1

    rh = table["restored_height"].to_a
    cd = table["current_distance"].to_a
    ci = table["cluster_id"].to_a

    min_h = nil
    max_h = nil

    min_d = nil
    max_d = nil

    while i < count - 1

      if rh[i].present? && cd[i].present? && ci[i] == "#{params[:id]}.csv"

        if (min_h.to_f > rh[i] && rh[i] != 0) || min_h == nil
          min_h = rh[i]
        end
        if (max_h.to_f < rh[i] && rh[i] != 0) || max_h == nil
          max_h = rh[i]
        end
        if (min_d.to_f > cd[i] && cd[i] != 0) || min_d == nil
          min_d = cd[i]
        end
        if (max_d.to_f < cd[i] && cd[i] != 0) || max_d == nil
          max_d = cd[i]
        end

        h = rh[i]
        d = cd[i]
        @xy << {distance: d, height: h}

      end

      i = i + 1

    end

    render json: {data: @xy, config: {height: {min: min_h, max: max_h}, distance: {min: min_d, max: max_d}}}

  end

end
