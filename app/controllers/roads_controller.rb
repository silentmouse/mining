class RoadsController < ApplicationController

  def index

    @roads = Dir.glob(Rails.root.join('public', 'roads','*')).map{|t| {name: t.split("/").last}}

    render json: {data: @roads}

  end

  def show


    @roads = Dir.glob(Rails.root.join('public', 'roads',params[:id],'*')).map{|t| {name: t.split("/").last}}

    render json: {data: @roads}

  end

  def gr


    table = Arrow::Table.load("#{Rails.root}/public/reports/#{params[:report_id]}.parquet")

    count = table.size

    @xy = []

    i = 1

    rh = table["restored_height"].to_a
    cd = table["current_distance"].to_a
    ci = table["cluster_id"].to_a

    min_h = nil
    max_h = nil

    min_d = nil
    max_d = nil

    while i < count - 1

      if rh[i].present? && cd[i].present? && ci[i] == "#{params[:id]}.csv"

        if (min_h.to_f > rh[i] && rh[i] != 0) || min_h == nil
          min_h = rh[i]
        end
        if (max_h.to_f < rh[i] && rh[i] != 0) || max_h == nil
          max_h = rh[i]
        end
        if (min_d.to_f > cd[i] && cd[i] != 0) || min_d == nil
          min_d = cd[i]
        end
        if (max_d.to_f < cd[i] && cd[i] != 0) || max_d == nil
          max_d = cd[i]
        end

        h = rh[i]
        d = cd[i]
        @xy << {distance: d, height: h}

      end
      i = i + 1
    end
    g = Gruff::Line.new(800,400)
    g.theme_pastel
    g.title = 'graphic'
    labels = {}
    @xy.each{|t| labels[t[:distance].to_s] = t[:distance]}
    g.labels = labels
    g.data params[:id].to_sym, @xy.map{|t| t[:height]}
    g.write('exciting.png')
  end

end
