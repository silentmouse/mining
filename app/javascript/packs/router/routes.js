

import Main from "../pages/Main"
import Other from "../pages/Other"
import Report from "../pages/Report";
import History from "../pages/History";

const routes = [
  {
    path: "/",
    component: Main,
    name: "MainPage",
  },
  {
    path: "/other",
    component: Other,
    name: "Other",
  },
  {
    path: "/reports/:id",
    component: Report,
    name: "Report",
  },
  {
    path: "/profiles/:id/history",
    component: History,
    name: "History",
  },

];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
